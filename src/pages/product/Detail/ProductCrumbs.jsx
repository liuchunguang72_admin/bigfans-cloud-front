/**
 * Created by lichong on 2/22/2018.
 */

import React from 'react';

import {Breadcrumb} from 'antd';
import {withRouter} from 'react-router-dom'

import HttpUtils from '../../../utils/HttpUtils';

class ProductCrumbs extends React.Component {

    state = {
        crumbs: []
    }

    componentDidMount() {
        var self = this;
        HttpUtils.getCrumbs({
            params: {
                prodId: this.props.match.params.id
            }
        }, {
            success: function (response) {
                if(response.data){
                    self.setState({crumbs: response.data.crumbs, prodName: response.data.prodName})
                }
            }
        })
    }

    render() {
        return (
            <div style={{marginTop:'10px' , marginBottom:'10px'}}>
                <Breadcrumb separator=">">
                    {
                        this.state.crumbs.map((crumb) => {
                            return (
                                <Breadcrumb.Item key={crumb.id} href={"/search?catId=" + crumb.id}>{crumb.name}</Breadcrumb.Item>
                            )
                        })
                    }
                    <Breadcrumb.Item>{this.state.prodName}</Breadcrumb.Item>
                </Breadcrumb>
            </div>
        );
    }
}

export default withRouter(ProductCrumbs);