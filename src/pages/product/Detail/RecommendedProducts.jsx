/**
 * Created by lichong on 2/22/2018.
 */

import React, {Component} from 'react';
import {Row, Col, Card , Radio , InputNumber , Button} from 'antd';

import HttpUtils from 'utils/HttpUtils'

class RecommendedProducts extends React.Component {

    state = {
        products : []
    }

    componentDidMount(){
        let self = this;
        HttpUtils.moreLikeThis(this.props.prodId , {
            success(resp){
                self.setState({products : resp.data.data})
            },
            error (resp) {

            }
        })
    }

    render() {
        return (
            <div>
                <h3>推荐商品</h3>
                <Card style={{ width: 213 }}>
                    {
                        this.state.products.map((prod , index) => {
                            return (
                                <div key={index}>
                                    <Row>
                                        <a href={'/product/' + prod.id}>
                                            <img style={{width: '150px' , height:'150px'}}
                                                 src={prod.imagePath}/>
                                        </a>
                                    </Row>
                                    <Row>
                                        <a href={'/product/' + prod.id}>{prod.name}</a><br/>
                                        <span>{prod.price}</span>
                                    </Row>
                                </div>
                            )
                        })
                    }
                </Card>
            </div>
        );
    }
}
export default RecommendedProducts;